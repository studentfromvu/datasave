<html>

<head>
    <title> Lexcicle Structure</title>

    <style>
        .pics ul li {
            display: inline;
            margin: 30px;
            margin-bottom: 40px;
        }
        
        p {
            margin-left: 10px;
            margin-right: 10px;
        }
        
        <!-- p {
            text-indent: 100px;
        }
        
        --> body {
            margin: 0;
            padding: 0;
            text-align: center;
            /* !!! */
            background: white;
        }
        
        .centered {
            margin: 0 auto;
            text-align: left;
            width: 900px;
            background: white;
        }
        
        .footerButton ul li {
            display: inline-block;
            float: right;
            margin-right: 50px;
        }
        
        #footer {
            background-color: #804E00;
            color: black;
            text-align: center;
            padding: 10px;
        }
        
        h2 {
            margin-left: 10px;
        }
        
        .box {
            margin-left: 0px;
            font-size: 20px;
            background: black;
            color: floralwhite;
        }
        
        .box p {
            border-color: red;
            border: 1px;
        }

    </style>
</head>

<body>
    <?php
    
    include 'index.php';
    
    ?>
        <div class="centered">



            <div class="pics">
                <ul>
                    <li>
                        <img src="Home.jpg" width="80" hieght="80">
                    </li>
                    <li>
                        <Font Size="8">
                
           Introduction
          </Font>
                    </li>
                    <li>

                        <img src="previus.jpg" width="80" hieght="80">

                    </li>
                    <li>

                        <img src="next.jpg" width="80" hieght="80">

                    </li>
                </ul>
            </div>



            <h2> C#</h2>

            <p>
                <Font Size="5">In this part of the C# tutorial, we will introduce the C# programming language.</Font>
            </p>

            <h2> Goal</h2>

            <p>
                <Font Size="5">The goal of this tutorial is to get you started with the C# programming language. The tutorial covers the core of the C# language, including variables, arrays, control structures and other core features. This tutorial uses command line compilers to build applications. It does not cover graphical interface development, or visual IDEs.</Font>
            </p>


            <h2> C#</h2>

            <p>
                <Font Size="5">C# is a modern, high-level, general-purpose, object-oriented programming language. It is the principal language of the .NET framework. It supports functional, procedural, generic, object-oriented, and component-oriented programming disciplines. The design goals of the language were software robustness, durability and programmer productivity. It can be used to create console applications, GUI applications, web applications, both on PCs and embedded systems. C# was created by Microsoft corporation. The name "C sharp" was inspired by musical notation where a sharp indicates that the written note should be made a semitone higher in pitch.<br>

C# is a very popular language. Currently, it is one of the top 10 popular languages in the world. It was created on the Windows platform. The Mono project has created a clone for the Linux and Mac platforms. C# is a compiled language. The source code is compiled into executable (.exe) files which are executed by the .NET platform.</Font>
            </p>


            <h2> .NET</h2>

            <p>
                <Font Size="5">The .NET framework is an application software platform from Microsoft, introduced in 2002 and commonly called .NET ("dot net"). It uses an intermediate bytecode language that can be executed on any hardware platform that has a runtime engine. Programs written for the .NET Framework execute in a software environment, known as the Common Language Runtime (CLR). Common Language Runtime is an application virtual machine that provides services such as security, memory management, and exception handling. The class library and the CLR together constitute the .NET Framework. The .NET framework supports several programming languages. In addition to C#, development is possible in Visual Basic .NET, managed C++, F#, IronPython or IronRuby.</Font>
            </p>

            <h2> Compiling</h2>

            <p>
                <Font Size="5">
There are two prominent C# compilers. The Microsoft C# compiler and the Mono C# compiler</Font>
            </p>

            <div class="box">
                <p>

                    using System;
                    <br> public class Simple
                    <br> {
                    <br> static void Main()
                    <br> {
                    <br> Console.WriteLine("This is C#");
                    <br> }
                    <br> }
                    <br>

                </p>
            </div>
            .
            <p>
                <Font Size="5">This is a simple C# program that prints a message to the console.<br>

On Linux, we need to install the Mono C# compiler. It is called gmcs for C# 3.0 profile and dmcs for the C# 4.0 profile. To install the Mono C# compiler, we must install the Mono platform.<br>

<Font Size="5"><b>$ dmcs simple.cs<br>
$ ./simple.exe <br>
    This is C#</b></Font>
                <br> We compile and run a simple C# program on Linux.
                <br> On Windows, we have two basic options. We either use the command line compiler or some version of the Visual Studio. The .NET framework is already installed on many versions of Windows OS. If not, we can download it and install it from the microsoft.com website.
                <br> On our system, the .NET framework was installed on C:\WINDOWS\Microsoft.NET\Framework\v3.5. Here we find the csc.exe file which is the compiler of the C# language.
                <br>

                <Font Size="5"><b>C:\programming\csharp>C:\WINDOWS\Microsoft.NET\Framework\v3.5\csc.exe simple.cs<br>
C:\programming\csharp>simple.exe<br>
This is C#<br>
    We compile and run a simple C# program on Windows.</b></Font>
                <br> Another option is to use a freely available Visual Studio C# Express Edition. We create a new project by selecting File/New Project or clicking Ctrl+N. From the available templates, we select a console application. (All programs in this tutorial are console programs.)</Font>
            </p>

            <img src="images/console.png">
            <p> <font color="#858585">Figure: Console application</font></p>

            <Font Size="5"> <p>To run an example, we press the Ctrl+F5 key combination.</p></Font>

            <h2>Source</h2>

            <p>
                <Font Size="5">The following sources were used to create this tutorial:</Font>
            </p>
            <p>
                <Font Size="5"> <a href="http://www.msdn.com/">msdn.com
                   </a></Font>
            </p>

            <p>
                <Font Size="5"> <a href="http://www.wikipedia.org/">wikipedia.org</a></Font>
            </p>



            <div class="footerButton">
                <ul>
                    <li>

                        <img src="next.jpg" width="80" hieght="80">

                    </li>
                    <li>

                        <img src="previus.jpg" width="80" hieght="80">

                    </li>

                </ul>
            </div>
            <img src="Home.jpg" width="80" hieght="80">


        </div>
        <div id="footer">

            ©copyright SoftoTechs
        </div>

</body>

</html>
