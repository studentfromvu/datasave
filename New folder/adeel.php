<html>

<head>

    <title>Learn C# Fastly
    </title>

    <style>
        #footer {
            background-color: #804E00;
            color: black;
            text-align: center;
            font-size: 20px;
            padding-bottom: 20px;
        }
        
        body {
            margin: 0;
            padding: 0;
            text-align: center;
            /* !!! */
            background: white;
        }
        
        .centered {
            margin: 0 auto;
            text-align: left;
            width: 900px;
            background: white;
        }
        
        #Section ul li {
            display: inline;
            padding: 60px;
        }
        
        #Section {
            width: 60%;
            align-content: center;
        }
        
        .contents a {
            text-decoration: none;
            padding: 30px;
            padding-top: 10px;
            font-size: 20px;
            font-weight: 600;
            display: inline-block;
            color: black;
        }
        
        <!-- p {
            text-indent: 100px;
        }
        
        --> .footerButton ul li {
            display: inline-block;
            float: right;
            margin-right: 50px;
        }
        
        button {
            width: 297px;
            height: 55px;
            border-radius: 10px;
            border-color: #86969C;
        }
        
        button a:hover {
            color: #0C1BF2;
        }
        
        p {
            margin-left: 10px;
            margin-right: 10px;
        }
        
        .pics ul li {
            display: inline;
            margin: 30px;
            margin-bottom: 40px;
        }

    </style>

</head>

<body>
    <div class="header">
        <?php
include 'index.php'

?>


    </div>
    <div class="centered">




        <div class="pics">




            <h1 align="center"><Font Size="8">C# CONTENTS</Font></h3>
          




            <p><font size="5">
                   This is C# tutorial. In this tutorial you will learn the basics and some advanced topics of the C# language. The tutorial is suitable for beginners and intermediate programmers.</font></p>
            <div class="contents">
                <button type="button"> <a href="file:///C:/Users/adeel/Desktop/new%20web/1st%20page.html#">Introduction in c#</a></button>
                <button class="button"> <a href="#"><b>Lexical structure</b></a></button>


                <button type="button" width="100px"> <a href="#">Basics        </a></button>

                <button type="button"> <a href="#">Data types</a></button>


                <button type="button"> <a href="#">Strings</a></button>


                <button type="button"> <a href="#"> C#  Operators </a></button>


                <button type="button"> <a href="#">Flow control</a></button>


                <button type="button"> <a href="#">Arrays  in  C#</a></button>

                <button type="button"> <a href="#">OOP I C#</a></button>


                <button type="button"> <a href="#">Methods</a></button>

                <button type="button"> <a href="#">OOP II in C#</a></button>


                <button type="button"> <a href="#">Properties</a></button>


                <button type="button"><a href="#">Structures</a></button>



                <button type="button"> <a href="#">Delegates</a></button>



                <button type="button"> <a href="#">Namespaces</a></button>

                <button type="button"><a href="#">Collections</a></button>


                <button type="button"> <a href="#">Input & outputs</a></button>

                <button type="button"> <a href="#">New features of C# 3.0</a></button>


                <button type="button"> <a href="#">Classes</a></button>
                <button type="button"> <a href="#">Interfaces   in   ( c# )</a></button>
                <button type="button"> <a href="#">Indexers in C#</a></button>
                <button type="button"> <a href="#">Enums c#</a></button>
                <button type="button"> <a href="#">Genrics</a></button>
                <button type="button"> <a href="#">Iterators</a></button>
                <button type="button"> <a href="#">Nullable types</a></button>
                <button type="button"> <a href="#">Events in C#</a></button>
                <button type="button"> <a href="#">Types Conversions</a></button>
                <button type="button"> <a href="#">Encapsulation in C#</a></button>
                <button type="button"> <a href="#">Overridings</a></button>
                <button type="button"> <a href="#">Overloading in c#</a></button>
                <button type="button"> <a href="#">Method hiding in c#</a></button>
                <button type="button"> <a href="#">Inheritence c#</a></button>
                <button type="button"> <a href="#">Struct in C#</a></button>
                <button type="button"> <a href="#">Exceptions</a></button>
                <button type="button"> <a href="#">Multithreading</a></button>






            </div>

            <div class="paragraph">
                <font size="10">     C# Language</font>
                <br><font size="5">

    <p>C# is a modern, high - level,   general - purpose,  object - oriented      programming language. It is the principal   language of the .NET   ramework.   The design goals of the language   were software   robustness,   durability and programmer productivity. It can be used to create console applications, GUI applications, web applications, bo  th on PCs or embedded systems.C# syntax is highly expressive, yet it is also simple and easy to learn. The curly-brace syntax of C# will be instantly recognizable to anyone familiar with C, C++ or Java. Developers who know any of these languages are typically able to begin to work productively in C# within a very short time.</p><p>C# is a very popular language. Currently, it is one of the top 10 popular languages in the world. It was created on the Windows platform. The Mono project has created a clone for the Linux and Mac platforms. C# is a compiled language. The source code is compiled into executable (.exe) files which are executed by the .NET platform.
    </p>
    
    <p>
    
    When the C# program is executed, the assembly is loaded into the CLR, which might take various actions based on the information in the manifest. Then, if the security requirements are met, the CLR performs just in time (JIT) compilation to convert the IL code to native machine instructions. The CLR also provides other services related to automatic garbage collection, exception handling, and resource management. Code that is executed by the CLR is sometimes referred to as "managed code," in contrast to "unmanaged code" which is compiled into native machine language that targets a specific system. The following diagram illustrates the compile-time and run-time relationships of C# source code files, the .NET Framework class libraries, assemblies, and the CLR.
     
    </p>
    
    </font>
                <p>
                    <img src="images/diagram.JPG">
                </p>
                <p><font size="5">
    Language interoperability is a key feature of the .NET Framework. Because the IL code produced by the C# compiler conforms to the Common Type Specification (CTS), IL code generated from C# can interact with code that was generated from the .NET versions of Visual Basic, Visual C++, or any of more than 20 other CTS-compliant languages. A single assembly may contain multiple modules written in different .NET languages, and the types can reference each other just as if they were written in the same language.
        </font>
                </p>

            </div>






            <div class="footerButton">
                <ul>
                    <li>

                        <a href="file:///C:/Users/adeel/Desktop/new%20web/1st%20page.html#"><img src="images/next.jpg" width="80" hieght="80"></a>

                    </li>
                    <li>

                        <a href="index.html"><img src="images/previus.jpg" width="80" hieght="80"></a>

                    </li>

                </ul>
            </div>
            <a href="index.html"><img src="images/Home.jpg" width="80" hieght="80"></a>

        </div>

        <a href="CSHARP.html"><img src="images/up%20Arrow.png" align="right" width="40" height="40"></a>
        <div id="footer">

            ©copyright SoftoTechs

        </div>

</body>

</html>
